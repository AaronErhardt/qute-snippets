class Benchmark {

  constructor(iterations = 1, delay = 200) {
    this.iter = iterations;
    this.curIter = 0;
    this.bm = [];
    this.delay = delay;
    this.results = [];
  }

  add(name, func) {
    this.bm.push({
      name,
      func
    });
  }

  run(i) {

    if (this.bm.length === 0)
      return;

    else if (i == undefined) {
      setTimeout(() => {
        this.run(0);
      }, this.delay);
      return;
    }

    let fn = this.bm[i].func;

    let result = this.exec(fn);

    this.results.push({
      name: this.bm[i].name,
      result: result,
      hue: Math.random() * 360
    });

    let td = qObj(d.cElem("tr")),
      name = qObj(d.cElem("td")),
      value = qObj(d.cElem("td"));

    name.textContent = this.bm[i].name;
    value.textContent = result;

    td.aChild(name);
    td.aChild(value);

    Q("#results").aChild(td);

    if (++i < this.bm.length)
      setTimeout(() => {
        this.run(i);
      }, this.delay);
    else
      this.showResults();
  }

  exec(fn) {

    this.curIter = this.iter;

    let start_time, end_time;

    start_time = performance.now();

    for (; this.curIter >= 0; --this.curIter) {
      fn();
    }

    end_time = performance.now();

    return ((end_time - start_time) / 1000).toFixed(3);
  }

  showResults() {

    console.table(this.results);

    let ctx = V('#resultChart').getContext('2d');
    let myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.results.map(arr => arr.name),
        datasets: [{
          label: 't in s',
          data: this.results.map(arr => arr.result),
          backgroundColor: this.results.map(arr => `hsl(${arr.hue}, 60%, 50%)`),
          borderColor: this.results.map(arr => `hsl(${arr.hue}, 30%, 50%)`),
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
            }]
        }
      }
    });
  }
}

let B = new Benchmark(10000); // 800000 interations

let body = Q("body"),
  vanillaElems = [],
  quteElems = [];

/*B.add("Nothing", () => {
  //quteElems2.push(d.cElem2("div"));
});*/

B.add("Qute create elem", () => {
  quteElems.push(d.cElem("div"));
});

B.add("Vanilla create elem", () => {
  vanillaElems.push(document.createElement("div"));
});

B.add("Qute append child", () => {
  body.aChild(quteElems[B.curIter]);
});

B.add("Vanilla append child", () => {
  body.appendChild(vanillaElems[B.curIter]);
});

B.add("Qute add event listener", () => {
  quteElems[B.curIter].cEv("click", () => {}, false);
});

B.add("Vanilla add event listener", () => {
  vanillaElems[B.curIter].addEventListener("click", () => {}, false);
});

B.add("Qute remove event listener", () => {
  quteElems[B.curIter].rEv();
});

/*B.add("Setup elements + IDs", () => {
  vanillaElems[B.curIter] = d.cElem("div");
  vanillaElems[B.curIter].id = "id" + B.curIter;
  body.aChild(vanillaElems[B.curIter]);
});

B.add("Qute add event listener", () => {
  vanillaElems[B.curIter].cEv("click", () => {});
});*/

w.cEv("load", () => {

  let cont = Q("#container");

  w.cEv("click", (ev) => {
    if (ev.target.tagName === "A") {
      return;
    }

    w.rEv();

    let info = V("#info");
    info.textContent = "Running benchmark...";

    B.run();

  });

  w.rEv("load");
});
