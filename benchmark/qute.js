/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//vanilla

function __aEv(ev, func, capt = false) {
  this.addEventListener(ev, func, capt);
}

function __cEv(name, func, capt = false) {
  this.ev.push({
    name: name,
    func: func,
    capt: capt
  });
  this.addEventListener(name, func, capt);
}

function __rEv(ev, func, capt = false) {

  if (ev === undefined) {
    for (let i = this.ev.length - 1; i >= 0; --i)
      this.removeEventListener(this.ev[i].name, this.ev[i].func, this.ev[i].capt);

    this.ev = [];

  } else if (func === undefined) {

    for (let i = this.ev.length - 1; i >= 0; --i) {
      if (ev === this.ev[i].name) {
        this.removeEventListener(ev, this.ev[i].func, this.ev[i].capt);
        this.ev.splice(i, 1);
        return;
      }
    }

  } else if (typeof (func) === "number") {
    this.removeEventListener(ev, this.ev[func], ev[func].capt);
    this.ev.splice(func, 1);

  } else {

    this.removeEventListener(ev, func, capt);
  }
}

function __A(q) {
  let t = this.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; --i) {
    c[i] = qObj(t[i]);
  }
  return c;
}

function __Q(q) {
  qObj(this.querySelector(q))
}

function __aChild(c) {
  this.appendChild(c)
}

function __rChild(c) {
  this.removeChild(c)
}

const __UNKNOWN_OBJ = "QUTE: Unknown object!";

const qObj = o => {

  if (o === null || o === undefined) {
    console.error(__UNKNOWN_OBJ, o);
    return o;
  }

  if (o.ev !== undefined)
    return o;


  o.ev = [];

  o.aEv = __aEv;

  o.rEv = __rEv;

  o.cEv = __cEv;

  //Element queries\\

  o.Q = __Q;

  o.A = __A;

  //DOM operations\\

  o.aChild = __aChild;

  o.rChild = __rChild;

  return o;
};

const w = qObj(window);

const d = qObj(document);

//d.cElem = e => d.createElement(e);

d.cElem = e => qObj(d.createElement(e));

d.cText = t => d.createTextNode(t);

const V = q => d.querySelector(q);

const Y = q => d.querySelectorAll(q);

//Qute

const Q = q => qObj(d.querySelector(q));

const A = q => {
  let t = d.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; i--)
    c[i] = qObj(t[i]);

  return c;
};
