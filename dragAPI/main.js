/* Qute dragAPI
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

class QuteDrag {
  constructor(container = ".dragContainer", draggable = "draggable") {

    this.update = () => {
      this.elems = this.container.A("." + draggable);
    };

    this.container = Q(container);
    this.elems = this.container.A("." + draggable);

    let event = {};

    if (/mobile|Android/i.test(window.navigator.userAgent)) {
      this.mobile = true;

      event.down = "touchstart";
      event.move = 'touchmove';
      event.up = 'touchend';
      event.cancel = 'touchcancel';

    } else {
      this.mobile = false;

      event.down = "mousedown";
      event.move = 'mousemove';
      event.up = 'mouseup';
      event.cancel = 'mouseleave';
    }

    this.active = false;
    this.target = undefined;
    this.top = 0;
    this.offset = 0;
    this.dragged = 0;

    this.container.aEv(event.down, ev => {
      if (this.mobile === true && ev.target.className !== 'mobile' && ev.target.parentNode.className !== 'mobile')
        return;

      ev.preventDefault();

      this.target = ev.target;

      while (this.target.className !== draggable)
        this.target = this.target.parentNode;

      this.dragged = this.elems.indexOf(this.target);
      if (this.dragged === -1) {
        console.error("Elements can't be added without updating the drag Object!", this.elems, this.target);
        return;
      }

      this.target.classList.add("active");

      const getClientY = ev => (ev.type.indexOf("touch") === -1) ? ev.clientY : ev.touches[0].clientY;

      let rect = this.target.getBoundingClientRect();
      this.top = rect.top;
      this.offset = this.top - getClientY(ev) - d.documentElement.scrollTop;

      if (this.mobile && this.vibrate)
        w.navigator.vibrate(30);
      else
        this.vibrate = true;

      let reset = (ev) => {
        w.rEv(event.up);
        w.rEv(event.cancel);
        w.rEv(event.move);

        for (let elem of this.elems) {
          this.container.aChild(elem);
          elem.moved = false;
          elem.style.top = "0px";
          elem.rEv("transitionend");
          elem.transition = false;
        }

        this.target.classList.remove("active");
      };

      w.cEv(event.up, reset);
      w.cEv(event.cancel, reset);


      let move = ev => {
        ev.preventDefault();

        this.target.style.top = getClientY(ev) - this.top + this.offset + d.documentElement.scrollTop + "px";

        if (this.dragged !== this.elems.length - 1) {
          const lower = this.elems[this.dragged + 1];

          if (lower.transition !== true) {

            const targetRect = this.target.getBoundingClientRect(),
              lowerRect = lower.getBoundingClientRect();

            if (targetRect.bottom > lowerRect.bottom - lowerRect.height * 0.4) {

              lower.transition = true;
              lower.cEv("transitionend", () => {
                lower.rEv("transitionend");
                lower.transition = false;
              });

              if (lower.moved === true)
                lower.style.top = "0px",
                lower.moved = false;
              else
                lower.style.top = -targetRect.height + "px",
                lower.moved = true;

              this.elems[this.dragged] = lower;
              this.elems[++this.dragged] = this.target;

              return;
            }
          }
        }

        if (this.dragged !== 0) {
          const upper = this.elems[this.dragged - 1];

          if (upper.transition !== true) {

            const targetRect = this.target.getBoundingClientRect(),
              upperRect = upper.getBoundingClientRect();

            if (targetRect.top < upperRect.top + upperRect.height * 0.4) {

              upper.transition = true;
              upper.cEv("transitionend", () => {
                upper.rEv("transitionend");
                upper.transition = false;
              });

              if (upper.moved === true)
                upper.style.top = "0px",
                upper.moved = false;
              else
                upper.style.top = targetRect.height + "px",
                upper.moved = true;

              this.elems[this.dragged] = upper;
              this.elems[--this.dragged] = this.target;
            }
          }
        }
      };

      w.cEv(event.move, move);
    });
  }
}

w.cEv("load", () => {

  w.rEv("load");

  let drag = new QuteDrag(".dragContainer", "answer");

});
